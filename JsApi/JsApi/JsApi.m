//
//  JsApi.m
//  Macs
//
//  Created by Chamindu R. Munasinghe on 6/11/12.
//  Copyright (c) 2012 CompareNetworks, Inc. All rights reserved.
//

#import "JsApi.h"

#define JS_PARAM_ASSETID @"assetId"

@implementation JsApi {
	NSString *_asset;
}

@synthesize delegate = _delegate;

-(BOOL) isApiRequest:(NSURLRequest *)request {
	return [request.URL.scheme isEqualToString:@"jsapi"];
}

-(NSString *)dispatch:(NSURLRequest *)request 
{
	NSString * callbackId = request.URL.host;
	NSString * script = [NSString stringWithFormat:@"jsapi.getCommand(\"%@\");", callbackId];
	NSString * json = [_delegate excecuteScript:script];
	NSData * jsonData = [json dataUsingEncoding:NSUTF8StringEncoding];
	NSError * error;
	id jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData
													options:kNilOptions error:&error];
	NSString * commandName = [jsonObject objectForKey:@"cmd"];
	
	SEL selector = NSSelectorFromString([NSString stringWithFormat:@"%@:result:", commandName]);
	
	if([self respondsToSelector:selector])
	{
		NSDictionary * arguments = [jsonObject objectForKey:@"args"];
		NSMutableDictionary * result = [[NSMutableDictionary alloc] init];
		NSMethodSignature * signature = [JsApi instanceMethodSignatureForSelector:selector];
		NSInvocation * invocation = [NSInvocation invocationWithMethodSignature:signature];
		[invocation setSelector:selector];
		[invocation setTarget:self];
		[invocation setArgument:&arguments atIndex:2];
		[invocation setArgument:&result atIndex:3];
		[invocation invoke];
		BOOL success;
		[invocation getReturnValue:&success];
		NSData * resultData = [NSJSONSerialization dataWithJSONObject:result options:kNilOptions error:&error];
		NSString * jsonResult = [[NSString alloc] initWithData:resultData encoding:NSUTF8StringEncoding]; 
		NSString * callbackScript;

		callbackScript = [NSString stringWithFormat:@"jsapi.callback(\"%@\", %@, %@);", callbackId, success ? @"true" : @"false", jsonResult];
		[_delegate excecuteScript:callbackScript];
	}
	

	return nil;
}

- (BOOL)viewAsset:(NSDictionary *)parameters result:(NSMutableDictionary *)result
{
	[self processParameters:parameters setResult:result];
	return [_delegate viewAsset:_asset];
}

- (BOOL) initApi:(NSDictionary *)parameters result:(NSMutableDictionary *)result
{
	NSNumber * apiLevel = [NSNumber numberWithInt:1];
	[result setObject:apiLevel forKey:@"apiLevel"];
	return YES;
}

- (void)processParameters:(NSDictionary *)parameters setResult:(NSMutableDictionary *)result
{
	_asset = [parameters objectForKey:JS_PARAM_ASSETID];
	[result setObject:_asset forKey:JS_PARAM_ASSETID]; 
}

@end
